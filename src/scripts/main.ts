
interface IMessage {
    text: string;
}

function printMessage(message: IMessage) {
    console.log('message', message.text);
}

printMessage({text: 'Hello world!'});