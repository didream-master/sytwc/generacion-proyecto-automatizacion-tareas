### Generación de proyecto
Tras instalar las dependencias iniciales yo y generator-learnfe mediante `npm install -g yo generator-learnfe`, se ejecutó `yo learnfe` para generar el proyecto, dando como resultado un proyecto con la siguiente estructura:
```
- src/
    - img/
    - scripts
        main.js
    - styles
        main.css
index.html
```
### Control de versiones
Para añadir al proyecto el control de versiones se ejecutó `git init` y posteriormente `git remote add origin REPO_URL`.

### Incorporación de gulp
Para incorporar gulp al proyecto fue necesario previamente añadir el gestor de paquetes npm mediante `npm init`, y posteriormente ejecutar `npm install --save-dev gulp` para instalar gulp.

### Incorporación BrowserSync
Se ha incorporado `browser-sync` como dependencia de desarrollo, añadido el siguiente código en el gulpfile:
```js
const gulp = require('gulp');
const browserSync = require('browser-sync').create();

function serve() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
}

exports.default = gulp.series(serve);
```
y exportado dicha tarea como tarea por defecto.

Para ejecutar el gulpfile se ha añadido el script npm `start` en el package.json:
```json
"scripts": {
    "start": "gulp"
},
```
de esta forma bastaría ejecutar `npm start`.


### Gestión de estilos css
Se ha incorporado sass como preprocesador. Para ello se ha instalado `gulp-sass` como dependencia de desarrollo e incorporado el siguiente código en gulpfile.js

```js
function styleSass() {
    return gulp.src('./src/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./dist'))
        .pipe(browserSync.stream());
}
// ...
exports.default = gulp.series(styleSass, serve);
```

de igual forma se ha puesto un observador en la función `serve` para observar los cambios en los ficheros .scss y ejecutar `styleSass` en consecuencia.

```js
gulp.watch('./src/**/*.scss', styleSass);
```

Posteriormente se ha añadido la minificación de los estilos, mediante la dependencia `gulp-clean-css`.

```js
const cleanCSS = require('gulp-clean-css');
// ...
function styleSass() {
    return gulp.src('./src/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCSS())
        .pipe(gulp.dest('./dist'))
        .pipe(browserSync.stream());
}
```

Y luego, la generación de sourcemaps de los estilos, mediante `gulp-sourcemaps`.

```js
const sourcemaps = require('gulp-sourcemaps');
// ...
function styleSass() {
    return gulp.src('./src/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.init())
        .pipe(cleanCSS())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./dist'))
        .pipe(browserSync.stream());
}
```

### Compresión de imágenes
Para comprimir las imagenes se ha usado la dependencia npm `gulp-imagemin` y añadido al gulpfile:

```js
const del = require('del');
const imagemin = require('gulp-imagemin');

// ...

function imageCompress() {
    return gulp.src('src/img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/img'));
}
// ...

exports.default = gulp.series(clean, gulp.parallel(styleSass, imageCompress), serve);
```
